@extends('layouts.dashboard')

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-lg-10 col-xl-8">
        <div id="forms">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('trip.store')}}" method="post">
                        @csrf

                        <div class="form-row">
                            <div class="col-12 col-md-6 mb-3">
                                <label for="symbol"> Départ</label>
                                <select required class="form-control" name="departure_town">
                                    <option value="">Choisir la ville de départ</option>
                                    @foreach ($towns as $item)
                                    <option value="{{$item->id}}"> {{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-12 col-md-6 mb-3">
                                <label for="symbol">Destination</label>
                                <select required class="form-control" name="destination_town">
                                    <option value="">Choisir la ville de destination</option>
                                    @foreach ($towns as $destination)
                                    <option value="{{$destination->id }}"> {{$destination->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price">Date de départ</label>
                            <input id="dat" class="form-control" type="date" name="departure_date" required>
                        </div>

                        <div class="form-row">
                            <div class="col-12 col-md-6 mb-3">
                                <label for="price">Nombre de places du bus</label>
                                <input class="form-control" type="number" name="seat" required>
                            </div>

                            <div class="col-12 col-md-6 mb-3">
                                <label for="price">Durée du trajet</label>
                                <input class="form-control" type="number" name="travel_time" required>    
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price">Prix du trajet</label>
                            <input class="form-control" type="number" name="price" required>
                        </div>

                        <button class="btn btn-primary" type="submit">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection