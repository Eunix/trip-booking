@extends('layouts.dashboard')

@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <section id="main-content">
                    <div class="row">
                        <div class="col-12 col-lg-12">
                          <div class="card">
                    
                            <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h4 class="card-header-title">Vue d'ensemble des clients</h4>
                                    </div>
                                </div>               
                            </div>
                    
                            <div class="table-responsive mb-0" 
                                data-list="{&quot;valueNames&quot;: [&quot;user-id&quot;, 
                                    &quot;user-name&quot;, &quot;user-phone&quot;, &quot;currency-owner&quot;,
                                    &quot;currency-address&quot;, &quot;currency-available&quot;]}">
                    
                              <table id="_overview" class="table table-bordered table-sm table-nowrap card-table">
                                
                                <thead>
                                  <tr>
                                    <th scope="col">
                                      <a href="#" class="text-muted list-sort" data-sort="cuuser-id">
                                        ID 
                                      </a>
                                    </th>
                    
                                    <th>
                                      <a href="#" class="text-muted list-sort" data-sort="user-name">
                                        Nom du client
                                      </a>
                                    </th>
                    
                                    <th>
                                      <a href="#" class="text-muted list-sort" data-sort="user-phone">
                                        Edresse email
                                      </a>
                                    </th>

                                  </tr>
                                </thead>
                    
                                <tbody class="list">
                                    @foreach ($users as $item)
                                    <tr>
                                        <td class="user-id">
                                          {{ $item->id }}
                                        </td>
                        
                                        <td class="user-name">
                                          {{ $item->name }}
                                        </td>
                        
                                        <td class="user-email">
                                          {{ $item->email }}
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                    
                              </table>
                            </div>
                    
                          </div>
                    
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection

@push('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
@endpush

@push('scripts')
    <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#_overview').DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
                }
            });
        } );
    </script>
@endpush