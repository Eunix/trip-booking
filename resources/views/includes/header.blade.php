<!-- start Header -->
<header id="header-waypoint-sticky" class="header-main header-mobile-menu with-absolute-navbar">

    <div class="header-outer clearfix">

        <div class="header-inner">

            <div class="row shrink-auto-lg gap-0 align-items-center">

                <div class="col-5 col-shrink">
                    <div class="col-inner">
                        <div class="main-logo">
                            <a href="/"><img src="{{ asset('assets/images/2.png') }}" alt="Logo" /></a>
                        </div>
                    </div>
                </div>
                @guest
                <div class="col-7 col-shrink order-last-lg">
                    <div class="col-inner">
                        <ul class="nav-mini-right">
                            <li class="d-none d-sm-block">
                                <a href="#loginFormTabInModal-register" data-toggle="modal" data-target="#loginFormTabInModal" data-backdrop="static" data-keyboard="false">
                                    <span class="icon-font"><i class="icon-user-follow"></i></span> Inscription
                                </a>
                            </li>
                            <li class="d-none d-sm-block">
                                <a href="#loginFormTabInModal-login" data-toggle="modal" data-target="#loginFormTabInModal" data-backdrop="static" data-keyboard="false">
                                    <span class="icon-font"><i class="icon-login"></i></span> Connexion
                                </a>
                            </li>
                            <li class="d-block d-sm-none">
                                <a href="#loginFormTabInModal-register" data-toggle="modal" data-target="#loginFormTabInModal" data-backdrop="static" data-keyboard="false">
                                    Connexion / Inscription
                                </a>
                            </li>
                            <li>
                                <button class="btn btn-toggle collapsed" data-toggle="collapse" data-target="#mobileMenu"></button>
                            </li>
                        </ul>
                    </div>
                </div>
                @endguest

                @auth
                    <div class="col-7 col-shrink order-last-lg">
                        {{ auth()->user()->name}}
                    </div>
                @endauth


                <div class="col-12 col-auto">

                    <div class="navbar-wrapper">

                        <div class="navbar navbar-expand-lg">

                            <div id="mobileMenu" class="collapse navbar-collapse">

                                <nav class="main-nav-menu main-menu-nav navbar-arrow">

                                    <ul class="main-nav">

                                        <li><a href="/">Accueil</a></li>

                                        <li><a href="/about-us">A propos de nous</a></li>

                                        <li><a href="/contact">Nous contacter</a></li>

                                    </ul>

                                </nav><!--/.nav-collapse -->

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</header>
<!-- start Header -->
