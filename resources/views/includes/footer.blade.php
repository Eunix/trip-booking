<!-- start Footer Wrapper -->
<footer class="footer-wrapper light scrollspy-footer">

    <div class="footer-top">

        <div class="container">

            <div class="row shrink-auto-md align-items-lg-center gap-10">

                <div class="col-12 col-shrink order-last-md">

                    <div class="col-inner">

                        <div class="footer-dropdowns">

                            <div class="row shrink-auto gap-30 align-items-center">



                                <div class="col-shrink">

                                    <div class="col-inner">

                                        <div class="dropdown dropdown-smooth-01 dropdown-currency">

                                            <div class="dropdown-menu" aria-labelledby="dropdownCurrency">
                                                <div class="dropdown-menu-inner">
                                                    <a class="dropdown-item" href="#"><span class="icon-font"><i class="oi oi-dollar text-primary mr-10"></i></span>XOF</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-12 col-auto">

                    <div class="col-inner">
                        <ul class="footer-contact-list">
                            <li>
                                <span class="icon-font text-primary inline-block-middle mr-5 font16"><i class="fa fa-phone"></i></span> <span class="font700 text-black">90494232 / 93842029</span> <span class="text-muted">Lun-Sam | 7h:30 - 19h:30</span>
                            </li>
                            <li>
                                <span class="icon-font text-primary inline-block-middle mr-5 font16"><i class="fa fa-envelope"></i></span> <span class="font700 text-black">support@gmail.com</span>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

            <hr class="opacity-7" />

        </div>

    </div>

    <div class="main-footer">

        <div class="container">

            <div class="row gap-50">

                <div class="col-12 col-lg-5">

                    <div class="footer-logo">
                        <img src="assets/images/2.png" alt="images" />
                    </div>

                    <p class="mt-20">Partez à la découverte du monde grâce à TripCheap!! Avec nos meilleures offres, vous pouvez partir en vacances en toute sérénité. </p>

                </div>

                <div class="col-12 col-lg-7">

                    <div class="col-inner">

                        <div class="row shrink-auto-sm gap-30">

                            <div class="col-6 col-shrink">

                                <div class="col-inner">
                                    <h5 class="footer-title">A propos de nous</h5>
                                    <ul class="footer-menu-list set-width">
                                        <li><a href="about-us">Qui nous sommes</a></li>

                                    </ul>
                                </div>

                            </div>

                            <div class="col-6 col-shrink">

                                <div class="col-inner">
                                    <h5 class="footer-title">Service Client</h5>
                                    <ul class="footer-menu-list set-width">

                                        <li><a href="contact">Nous Contacter</a></li>
                                        <li><a href="#">Nos Services</a></li>

                                    </ul>
                                </div>

                            </div>

                            <div class="col-12 col-auto">

                                <div class="col-inner">
                                    <h5 class="footer-title">Newsletter &amp; Social</h5>
                                    <p class="font12">Abonnez-vous à notre newsletter pour resté informés sur les bon plans.</p>
                                    <form class="footer-newsletter mt-20">
                                        <div class="input-group">
                                            <input type="email" class="form-control" placeholder="Adresse email">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button"><i class="far fa-envelope"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="footer-socials mt-20">
                                        <a href="#"><i class="fab fa-facebook-square"></i></a>

                                        <a href="#"><i class="fab fa-google-plus-square"></i></a>
                                        <a href="#"><i class="fab fa-whatsapp"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="bottom-footer">

        <div class="container">

            <div class="row shrink-auto-md gap-10 gap-40-lg">


                <div class="col-shrink">
                    <div class="col-inner">
                        <p class="footer-copy-right"> &#169; {{ now()->year }} <span class="text-primary">{{ config('app.name', 'Laravel') }} LTd</span> Tous droits reservés.</p>
                    </div>
                </div>

            </div>

        </div>

    </div>

</footer>
<!-- start Footer Wrapper -->
