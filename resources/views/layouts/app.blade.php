<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Font face -->
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;display=swap" rel="stylesheet"> 
	<link href="{{ asset('assets/font-faces/metropolis/metropolis.css') }}" rel="stylesheet"> 
	
	<!-- CSS Plugins -->
	<link href="{{ asset('assets/css/font-icons.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" media="screen">	
	<link href="{{ asset('assets/css/animate.html') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/main.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/css/plugin.css') }}" rel="stylesheet">
	
	<!-- CSS Custom -->
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
	
	<!-- CSS Custom -->
	<link href="{{ asset('assets/css/your-style.css') }}" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="with-waypoint-sticky" id="myBody">
    <div id="app">
        @yield('content')
    </div>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/custom-core.js') }}"></script>
	@stack('scripts')
</body>
</html>