@extends('layouts.app')

@section('content')


	<!-- start Body Inner -->
	<div class="body-inner">

		@include('includes.header')

		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">

			<div class="hero-banner hero-banner-01 overlay-light opacity-2 overlay-relative overlay-gradient gradient-white alt-option-03" style="background-image:url('assets/images/image-bg/01.jpg'); background-position: top center;">

				<div class="overlay-holder bottom"></div>

				<div class="hero-inner">

					<div class="container">
						<h1>Le <span class="font200">tour <span class="block">avec <span class="font700">Trip</span>Cheap </span></span></h1>
						<p class="font-lg spacing-1">Voyager à travers tous les pays d'Afrique</p>

						<div class="search-form-main">
							<form method="GET" action="{{ route('search.trip')}} ">
								<div class="from-inner">

									<div class="row shrink-auto-sm gap-1">

										<div class="col-12 col-auto">
											<div class="col-inner">
												<div class="row cols-1 cols-sm-3 gap-1">

													<div class="col">
														<div class="col-inner">
															<div class="form-group">
																<label>Ville de départ</label>
																<select name="departure_town" class="chosen-the-basic form-control form-control-sm" placeholder="Veuillez choisir une ville" tabindex="2">
																	<option>Choisir la ville de départ</option>
																	@foreach ($towns as $item)
																	<option value="{{$item->id}}" >{{ $item->name}} </option>
																	@endforeach
																</select>
															</div>
														</div>
													</div>

													<div class="col">
														<div class="col-inner">
															<div class="form-group">
																<label>Destination</label>
																<select class="chosen-the-basic form-control form-control-sm" placeholder="Veuillez choisir" name="destination_town" tabindex="2">
																	<option>Choisir la destination</option>
																	@foreach ($towns as $destination)
																	<option value="{{$destination->id}}" >{{ $destination->name}} </option>
																	@endforeach
																</select>
															</div>
														</div>
													</div>

													<div class="col">
														<div class="col-inner">
															<div class="form-group">
																<label>Date de départ</label>
																<input type="date" name="departure_date" class="form-control" >
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>

										<div class="col-12 col-shrink">
											<div class="col-inner">
												<a href="result"><button type="button" class="btn btn-primary btn-block"><i class="ion-android-search"></i></button></a>
											</div>
										</div>

									</div>
								</div>
							</form>
						</div>

					</div>

				</div>

			</div>

			<section class="pb-0">

				<div class="container">

					<div class="row cols-1 cols-lg-3 gap-20 gap-lg-40">

						<div class="col">
							<div class="featured-icon-horizontal-01 clearfix">
								<div class="icon-font">
									<i class="elegent-icon-gift_alt text-primary"></i>
								</div>
								<div class="content">
									<h6>Nos meilleures offres</h6>
									<p class="text-muted">Nous vous facilitons les déplacements avec de meilleures offres </p>
								</div>
							</div>
						</div>

						<div class="col">
							<div class="featured-icon-horizontal-01 clearfix">
								<div class="icon-font">
									<i class="elegent-icon-wallet text-primary"></i>
								</div>
								<div class="content">
									<h6>Meilleur prix garanti</h6>
									<p class="text-muted">Vous ne devez pas toujours vous ruiner pour partir en voyage et la meilleure façon d’économiser sur vos voyages est d’être flexible. </p>
								</div>
							</div>
						</div>

						<div class="col">
							<div class="featured-icon-horizontal-01 clearfix">
								<div class="icon-font">
									<i class="elegent-icon-heart_alt text-primary"></i>
								</div>
								<div class="content">
									<h6>Nos passagers, notre métier</h6>
									<p class="text-muted">Nos passagers nous font confiance, leur faire plaisir est notre priorité</p>.
								</div>
							</div>
						</div>

					</div>

					<div class="clear mb-100"></div>



					<div class="section-title">
						<h2><span><span>Nos</span> Trajets programmés</span></h2>
					</div>

					<div class="row equal-height cols-1 cols-sm-2 cols-lg-3 gap-20 mb-30">

						@foreach ($trips as $item)
						<div class="col">

							<figure class="tour-grid-item-01">

								<a href="{{ route('booking.create', $item->id)}}">

									<div class="image">
										<img src="assets/images/bus1.jpg" alt="images" />
									</div>

									<figcaption class="content">
										<h5>Trajet {{ $item->departure->name}} - {{ $item->destination->name }} </h5>

										<ul class="item-meta mt-15">
											<li><span class="font700 h6">{{ $item->travel_time}} heures de route</span></li>
											<li><span class="font700 h6">{{ $item->seat}} Places disponibles</span></li>
											<li>
												Départ: <span class="font600 h6 line-1 mv-0"> {{ $item->departure->name}}</span> - Destination: <span class="font600 h6 line-1 mv-0"> {{ $item->destination->name }}</span>
											</li>
										</ul>
										<p class="mt-3">Prix à partir de: <span class="h6 line-1 text-primary font16"> {{$item->price}} CFA</span> <span class="text-muted mr-5"></span></p>
									</figcaption>

								</a>

							</figure>

						</div>
						@endforeach

					</div>

				</div>

			</section>

			<div class="bg-white-gradient-top-bottom pt-0 mt-40">

				<div class="bg-gradient-top"></div>
				<div class="bg-gradient-bottom"></div>
				<div class="bg-image pv-100 overlay-relative" style="background-image:url('assets/images/image-bg/44.jpg');">
					<div class="overlay-holder overlay-white opacity-8"></div>
					<div class="container"></div>
				</div>

				<div class="overlay-relative overlay-gradient gradient-white set-height-01">
					<div class="overlay-holder bottom"></div>
				</div>

			</div>



		</div>
		<!-- end Main Wrapper -->


		@include('includes.footer')

	</div>
	<!-- end Body Inner -->

	@guest
		@include('includes.auth-modal')
	@endguest


	<!-- start Back To Top -->
	<a id="back-to-top" href="#" class="back-to-top" role="button" title="Click to return to top" data-toggle="tooltip" data-placement="left"><i class="elegent-icon-arrow_carrot-up"></i></a>
	<!-- end Back To Top -->
@endsection
