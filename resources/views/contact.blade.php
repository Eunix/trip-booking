@extends('layouts.app')

@section('content')

<div class="body-inner">
    @include('includes.header')
    <div class="main-wrapper scrollspy-container">

        <section class="page-wrapper page-result pb-0">

            <div class="container pv-60">

                <div class="mb-50"></div>

                <div class="row gap-50 gap-lg-0">

                    <div class="col-12 col-lg-5 col-xl-4">

                        <h4 class="heading-title"><span>Envoyer <span class="font200">un message:</span></span></h4>

                        <form id="contact-form" method="post" action="contact-2.php">

                            <div class="contact-successful-messages"></div>

                            <div class="contact-inner">

                                <div class="form-group">
                                    <label for="form_name">Nom complet *</label>
                                    <input id="form_name" type="text" name="name" class="form-control" required="required" data-error="Your name is required.">
                                    <div class="help-block with-errors text-danger"></div>
                                </div>

                                <div class="form-group">
                                    <label for="form_email">Adresse mail *</label>
                                    <input id="form_email" type="email" name="email" class="form-control" required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors text-danger"></div>
                                </div>

                                <div class="form-group">
                                    <label for="form_subject">Objet</label>
                                    <input id="form_subject" type="text" name="email" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="form_message">Message *</label>
                                    <textarea id="form_message" name="message" class="form-control" rows="7s" required="required" data-error="Please,leave us a message."></textarea>
                                    <div class="help-block with-errors text-danger"></div>
                                </div>

                                <input type="submit" class="btn btn-primary btn-send btn-wide mt-15" value="Envoyer message">

                            </div>

                        </form>

                    </div>

                    <div class="col-12 col-lg-6 ml-auto">

                        <h4 class="heading-title"><span>informationtions de <span class="font200">contact:</span></span></h4>

                        <ul class="contact-list-01">

                            <li>
                                <span class="icon-font"><i class="ion-android-pin"></i></span>
                                <h6>Adresse</h5>
                                Gblinkomè<br/>Carrefour de l'amitié <br/>Togo,Lomé
                            </li>

                            <li>
                                <span class="icon-font"><i class="ion-android-mail"></i></span>
                                <h6>Email</h5>
                                <a href="#">support@gmail.com</a>
                            </li>

                            <li>
                                <span class="icon-font"><i class="ion-android-call"></i></span>
                                <h6>Téléphone</h5>
                                    (228) 90494232
                            </li>

                            <li>
                                <span class="icon-font"><i class="ion-android-print"></i></span>
                                <h6>Téléphone</h5>
                                (228) 93842029
                            </li>

                        </ul>

                        <div class="mb-50"></div>

                        <h4 class="heading-title"><span>Suivez  <span class="font200">nous sur:</span></span></h4>

                        <div class="social-btns-01">
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-linkedin"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fab fa-instagram"></i></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Codepen"><i class="fab fa-codepen"></i></a>
                            <a href="#" data-toggle="tooltip " data-placement="top" title="Whatsapp"><i class="fab fa-whatsapp"></i></a>
                        </div>

                    </div>

                </div>

            </div>

        </section>

    </div>
    @include('includes.footer')
</div>

@endsection

@push('scripts')
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endpush
