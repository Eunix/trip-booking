<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\WelcomeController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/trip', [App\Http\Controllers\TripController::class, 'index'])->name('trip.index');
Route::get('/trip/create', [App\Http\Controllers\TripController::class, 'create'])->name('trip.create');
Route::post('/trip/create', [App\Http\Controllers\TripController::class, 'save'])->name('trip.store');

Route::get('/client', [App\Http\Controllers\HomeController::class, 'userIndex'])->name('client.index');

Route::get('/booking', [App\Http\Controllers\BookingController::class, 'index'])->name('booking.index');

Route::get('/booking/create/{id}', [App\Http\Controllers\BookingController::class, 'create'])->name('booking.create')->middleware('auth');



Route::get('/search', [App\Http\Controllers\WelcomeController::class, 'searchTrip'])->name('search.trip');

Route::get('/contact', function() {
    return view('contact');
});

Route::get('/about-us', function() {
    return view('about');
});
Route::get('/result', function() {
    return view('result');
});
