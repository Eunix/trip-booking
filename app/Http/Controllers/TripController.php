<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Town;
use App\Models\Trip;

class TripController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Méthode qui affiche la liste des trajets disponibles sur la plateforme
    public function index() {
        $index = Trip::with('departure', 'destination')->get();
        return view('dashboard.trip.index', \compact('index'));
    }

    // Méthode pour afficher la page de création ou d'ajout d'un nouveau trip
    public function create(){
        $towns = Town::orderBy('name')->get();
        return view('dashboard.trip.create', \compact('towns'));
    }
    
    // Function pour enregistrer un trajet
    // Elle est urilisée par l'adminstrateur
    public function save(Request $request) {
        $trip = Trip::create([
            'town_id' => $request->input('departure_town'),
            'departure_date' => $request->input('departure_date'),
            'seat' => $request->input('seat'),
            'travel_time' => $request->input('travel_time'),
            'destination_id' => $request->input('destination_town'),
            'price' => $request->input('price')
        ]);

        return back()->with('success', 'Opération effectué avec succès');
    }

}
