<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Booking;
use App\Models\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->role == 'admin') {
            $users = User::where('role', 'user')->get();
            $booking = Booking::where('is_done', false)->get();

            return view('dashboard.home', \compact('users', 'booking'));
        } 

        return redirect('/');
    }

    // Méthode qui affiche la liste des clients à l'adminstrateur
    public function userIndex() {
        $users = User::where('role', 'user')->get();
        return view('dashboard.clients.index', \compact('users'));
    }
}
