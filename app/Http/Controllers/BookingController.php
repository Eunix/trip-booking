<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Trip;

use Illuminate\Http\Request;


class BookingController extends Controller
{
    // Méthode pour afficher la liste des 
    public function index() {
        $books = Booking::latest()->get();
        return view('dashboard.booking.index', \compact('books'));
    }

    //Méthode qui enregistre un nouvel reservation
    public function create($id) {
        $trip = Trip::find($id);
        
        return view('payment', \compact('trip'));

    }


    public function adminUpdateBooking($id) {
        Booking::where('id', $id)->update([
            'is_done' => true
        ]);

        return back()->with('success', 'Opération effectué avec succès');
    }
}
