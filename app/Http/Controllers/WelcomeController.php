<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Town;
use App\Models\Trip;

use Carbon\Carbon;


class WelcomeController extends Controller
{
    public function index() {
        $towns = Town::orderBy('name')->get();
        $trips = Trip::with('departure', 'destination')->latest()->limit(6)->get();
        return view('welcome', \compact('towns', 'trips')); 
    }

    public function searchTrip(Request $request){
        $departure = Town::find($request->input('departure_town'));
        $destination = Town::find($request->input('destination_town'));
        $dt = Carbon::now()->toDateString();

        $trip = Trip::where([
            ['town_id', $departure],
            ['destination_id', $destination],
            ['departure_date', [$dt, $request->input('departure_date')]]
        ])->get();

        dd($trip);
        return view('result');
    }
        
}
