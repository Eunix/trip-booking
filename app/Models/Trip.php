<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;

    protected $fillable = [
        'town_id',
        'departure_date',
        'seat',
        'travel_time',
        'destination_id',
        'price',
        'is_done'
    ];

    public function destination() {
        return $this->belongsTo('App\Models\Town', 'destination_id');
    }

    public function departure() {
        return $this->belongsTo('App\Models\Town', 'town_id');
    }

}
