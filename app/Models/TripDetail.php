<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TripDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'trip_id',
        'town_id',
        'duration',
        'is_last',
    ];

    public function trip() {
        return $this->belongsTo('App\Models\Trip');
    }
}
