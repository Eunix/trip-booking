<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('trip_id')->nullable()->constrained(); // ID du trajet
            $table->foreignId('town_id')->nullable()->constrained(); // ID de la ville d'arrêt => renseigne la ville de l'arrêt
            $table->unsignedInteger('duration')->nullable(); // Renseigne le nombre de minutes à faire durant l'arrêt
            $table->boolean('is_last')->default(false); // Renseigne si l'arrêt en question est le dernier du trajet
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_details');
    }
}
