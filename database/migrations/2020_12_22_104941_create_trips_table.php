<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
            $table->foreignId('town_id')->constrained(); // Rensigne la ville de départ
            $table->unsignedBigInteger('destination_id');
            $table->date('departure_date'); // Renseigne la date de départ
            $table->unsignedInteger('seat'); // Renseigne le nombre de place disponible
            $table->unsignedInteger('travel_time'); // Renseigne la durée du trajet
            $table->unsignedInteger('price')->nullable(); // Renseigne le prix du trajet
            $table->boolean('is_done')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
